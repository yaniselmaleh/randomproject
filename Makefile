start:
	- docker-compose up

compile:
	- docker-compose build

up-build:
	- docker-compose up --build

stop:
	- docker-compose down
react-build:
	- docker-compose exec react npm run build

react-build-no-map:
	- docker-compose exec react npm run build-no-map